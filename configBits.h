/*! \file  configBits.h
 *
 *  \brief Configuration fuses for the PIC24FV16KM202
 *
 *  This file, which is only referenced in the mainline, sets the
 *  configuration bits for the PIC.  These bits determine such things
 *  as what oscillator to use, whether oscillator switching is permitted,
 *  and whether to enable the watchdog timer and brownout detection.
 *
 *  In this application, the watchdog timer is used, and the primary
 *  internal RC oscillator is selected with the PLL and postscaler.
 *
 *  The configuration bits are set as follows:
 *  \li _FOSCSEL \c FNOSC_FRCPLL Select the fast RC oscillator with PLL and Postscaler
 *  \li _FOSCSEL \c LPRCSEL_LP Set the oscillator to low power, low accuracy mode
 *  \li _FOSCSEL \c IESO_ON Allow oscillator switching
 *  \li _FOSC \c POSCFREQ_HS Primary oscillator/external clock input frequency between 100kHz and 8MHz
 *  \li _FOSC \c SOSCSEL_SOSCLP Secondary Oscillator configured for low-power operation
 *  \li _FOSC \c FCKSM_CSECMD Clock Switching is enabled, Fail-safe Clock Monitor is disabled
 *  \li _FWDT \c FWDTEN_ON  WDT enabled in hardware
 *  \li _FWDT \c WDTPS_PS8192 WDT postscaler 1:8192 (approx 60 sec.)
 *  \li _FWDT \c FWPSA_PR128 WDT prescaler ratio of 1:128 (4ms step)
 *
 *  The bits are set using provided macros.
 *
 *  Since the application manipulates the clocks, some discussion of the
 *  available clocks is in order.  The primary clock can be driven by a crystal
 *  or an onboard 8MHz RC clock.  In this application, the internal
 *  RC clock has been selected as timing is not critical.  The primary clock
 *  may optionally be followed by a PLL which multiplies the clock by 4.
 *  Since the PIC executes one instruction every 2 clocks, that results
 *  in 16 million instructions per second (16 MIPS).  The primary clock
 *  may optionally be followed by a postscaler, allowing many options for
 *  slower clocks.  In this application, we choose 1 MIPS and 3.9 KIPS.
 *
 *  In addition to the primary clock, there is a 32 kHz secondary clock
 *  which may be used as the processor clock, and/or the input to a watchdog
 *  timer.  The watchdog timer has a prescaler and a postcaler, allowing
 *  for watchdog timeouts between 1 millisecond and 4 minutes.  In this
 *  application, we will use a watchdog timeout of approximately one minute.
 *
 *  \author jjmcd
 *  \date December 28, 2014, 2:52 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef CONFIGBITS_H
#define	CONFIGBITS_H

#ifdef	__cplusplus
extern "C"
{
#endif

/*! Primary oscillator selection */
_FOSCSEL( FNOSC_FRCPLL & // Fast RC Oscillator with Postscaler and PLL Module (FRCDIV+PLL)
          LPRCSEL_LP     // Low Power, Low Accuracy Mode
        )
/*! Clock hinting and secondary oscillator */
_FOSC( POSCFREQ_MS &     // Primary oscillator/external clock input frequency between 100kHz and 8MHz
       SOSCSEL_SOSCLP    // Secondary Oscillator configured for low-power operation
     )
/*! Watchdog timer */
_FWDT( FWDTEN_ON &       // WDT enabled in hardware
       WDTPS_PS8192 &    // 60 sec WDT postscaler 1:8192
       FWPSA_PR128       // WDT prescaler ratio of 1:128
       )

#ifdef	__cplusplus
}
#endif

#endif	/* CONFIGBITS_H */

