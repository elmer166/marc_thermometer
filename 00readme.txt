/*! \file 00readme.txt
 *  \brief Digital Thermometer
 *
 *  \page 1intro Introduction
 *
 *  This paper describes the software to be used for the Midland Amateur
 *  Radio Club group build of a digital thermometer.  The project will
 *  be constructed on a perfboard using traditional point-to-point
 *  techniques.
 *  
 *  \section circuit The circuit
 *
 * The temperature is sensed by a Dallas Semiconductor DS1821 Programmable
 * Digital Thermostat.  The DS1821 uses a "single-wire" protocol which is
 * read by a PIC24FV16KM202 microcontroller.  The result is displayed on
 * a standard 16x2 character LCD module.  The circuit is powered by 3 AA
 * batteries. An LED is included to indicate when a temperature conversion
 * is taking place.
 *
 * Generally, microcontroller circuits are either 5 volt or 3 volt circuits,
 * although most microcontrollers can operate over a wide range of voltages.
 * In the case of "5 volt" PICs, the range is typically around 2.5 to 5.5
 * volts.  There are "3 volt" PICs which can operate from typically 3.6
 * down to 1.8 volts.
 *
 * For a battery operated project it would seem that 3 volts would be a
 * good choice, but often one wants a display, and there is a problem;
 * "3 volt" LCDs tend to be a little expensive, and 3 volts is actually
 * 3.3 volts nominal.  3 volt LCDs fade quickly as the voltage drops
 * below 3 volts, so while the circuit may continue to operate for some time,
 * the display would become invisible after just a few hours.  While one could
 * use a voltage regulator to reduce 4.5 volts of battery to 3.3, regulators
 * tend to waste a lot of power, not good for a battery powered project.
 *
 * However, 5 volt LCDs operate fine at 4.5 volts and below, so a "5 volt"
 * circuit running from 3-1.5 volt batteries seems like a good choice.
 *
 * The PIC24FV16KM202 was chosen as the processor because:
 * \li As a PIC24, it consumes relatively little power, making it good for
 *     a battery powered project
 * \li It is one of few 5 volt PIC24 models
 * \li It is available in a hobbyist-friendly DIP package
 * \li It is quite inexpensive
 * \li It has a variety of peripherals which may encourage the builder
 *     to further experimentation
 * \li It has sufficient memory that it can be programmed in a high level
 *     language without undue concern over resources
 * \li It has a variety of power saving features
 *
 *  While the 16 character by two line LCD is more than is needed for this
 *  project, it is a common format, easily available, and offers the builder
 *  additional flexibility for future experiments.
 *  
 *  The schematic for the circuit is shown below:
 *  \image latex "Thermometer-3.eps" "Circuit Schematic" width=17cm
 *
 *  \section software Software overview
 *
 *  The application runs on a PIC24FV16KM202 microcontroller and is programmed
 *  in C.
 *
 *  The application first initializes the various devices, then goes into
 *  a loop of reading the temperature, doing some calculations, displaying
 *  the results, and going to sleep.  A watchdog timer wakes the microcontroller
 *  after it has been sleeping for about a minute.
 *
 *  Refer to the
 *  \ref main_diagram
 *  in the mainline section for a diagram of the overall program flow.
 *
 *  Two external libraries are used which are not described here.  The DS1821
 *  routines take care of communicating with the Dallas Semiconductor DS1821
 *  Programmable Thermostat, and the LCD routines take care of display with
 *  the standard, Hitachi-compatible 16x2 liquid crystal display module.
 *
 *  \image latex "TemperatureDisplay.jpg" "Image of display" width=13cm
 *
 *  The application is broken into a number of small functions.  The diagram
 *  below shows the call graph.  Blue bubbles represent LCD functions, orange
 *  DS1821.  This document describes the white bubbles.  Standard C library
 *  functions (Sleep, sprintf) are not described.
 *  \dot
 *  digraph callgraph
 *  {
 *  	fontname="Helvetica-Bold";
 *  	label="Call Graph";
 *  	node [ fontname="Helvetica" ];
 *  	main [ shape="record" ];
 *  	LCDclear [ style="filled" fillcolor="lightcyan" ];
 *  	LCDinit [ style="filled" fillcolor="lightcyan" ];
 *  	LCDposition [ style="filled" fillcolor="lightcyan" ];
 *  	LCDputs [ style="filled" fillcolor="lightcyan" ];
 *  	DS1821_ReadTemp	 [ style="filled" fillcolor="blanchedalmond" ];
 *  	DS1821_Initialize	 [ style="filled" fillcolor="blanchedalmond" ];
 *  	main->Initialize;
 *  	main->LCDclear;
 *  	main->getTemp;
 *  	main->doCalculations;
 *  	main->displayResult;
 *  	getTemp->DS1821_ReadTemp;
 *  	getTemp->slowOsc;
 *  	getTemp->fastOsc;
 *  	Initialize->fastOsc;
 *  	Initialize->LCDinit;
 *  	Initialize->DS1821_Initialize;
 *  	displayResult->LCDposition;
 *  	displayResult->LCDputs;
 *  }
 *  \enddot
 *
 *  In order to maximize battery life, the 1821 is read at as slow a clock
 *  as can be used to deal with its timing requirements, and all the
 *  calculations and display are done at an even slower clock, chosen so
 *  that the update of the LCD is not annoyingly slow, although it is
 *  far from instant.
 *
 *  The PIC24FV16KM202 draws about 880uA at the faster clock, about 200uA
 *  at the slower clock, and a stingy 7uA when sleeping.
 *
 *  \date December 28, 2014, 11:08 PM
 */
