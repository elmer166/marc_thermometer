#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=Thermometer.X.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=thermometer.x.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/thermometer.x.tar
# Blue configuration
CND_ARTIFACT_DIR_Blue=dist/Blue/production
CND_ARTIFACT_NAME_Blue=Thermometer.X.production.hex
CND_ARTIFACT_PATH_Blue=dist/Blue/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_Blue=${CND_DISTDIR}/Blue/package
CND_PACKAGE_NAME_Blue=thermometer.x.tar
CND_PACKAGE_PATH_Blue=${CND_DISTDIR}/Blue/package/thermometer.x.tar
# Red configuration
CND_ARTIFACT_DIR_Red=dist/Red/production
CND_ARTIFACT_NAME_Red=Thermometer.X.production.hex
CND_ARTIFACT_PATH_Red=dist/Red/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_Red=${CND_DISTDIR}/Red/package
CND_PACKAGE_NAME_Red=thermometer.x.tar
CND_PACKAGE_PATH_Red=${CND_DISTDIR}/Red/package/thermometer.x.tar
# Simulate configuration
CND_ARTIFACT_DIR_Simulate=dist/Simulate/production
CND_ARTIFACT_NAME_Simulate=Thermometer.X.production.hex
CND_ARTIFACT_PATH_Simulate=dist/Simulate/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_Simulate=${CND_DISTDIR}/Simulate/package
CND_PACKAGE_NAME_Simulate=thermometer.x.tar
CND_PACKAGE_PATH_Simulate=${CND_DISTDIR}/Simulate/package/thermometer.x.tar
# Blue__2 configuration
CND_ARTIFACT_DIR_Blue__2=dist/Blue__2/production
CND_ARTIFACT_NAME_Blue__2=Thermometer.X.production.hex
CND_ARTIFACT_PATH_Blue__2=dist/Blue__2/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_Blue__2=${CND_DISTDIR}/Blue__2/package
CND_PACKAGE_NAME_Blue__2=thermometer.x.tar
CND_PACKAGE_PATH_Blue__2=${CND_DISTDIR}/Blue__2/package/thermometer.x.tar
